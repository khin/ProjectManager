#include <gtkmm.h>
#include <iostream>
#include "UI.hpp"

int main(int argc, char* argv[]){
	auto app = Gtk::Application::create(argc, argv, "com.khin.ProjectManager");

	std::cout << "Loading UI..." << std::endl;
	UI ui = UI("/ui.glade");

	return app->run(ui.GetMainWindow());
}
