#include "UI.hpp"
#include <iostream>

// ########## Constructor
UI::UI(std::string resource){
	main_window = nullptr;
	new_project_window = nullptr;
	create_class_window = nullptr;

	loaded = false;

	try{
		builder = Gtk::Builder::create_from_resource(resource);
	}
	catch(const Glib::Error& ex)
	{
		std::cerr << "Error: " <<  ex.what() << std::endl;
		return;
	}

	builder->get_widget("main_window", main_window);
	builder->get_widget("new_project_window", new_project_window);

	if (!main_window || !new_project_window){
		std::cout << "Failed to load UI" << std::endl;
	}
	else{
		main_window->set_title("Project Manager");
		main_window->resize(300, 100);
		ConnectSignals();

		loaded = true;

		manager = new ProjectManager();
	}
}

void UI::ConnectSignals(){
	Gtk::ImageMenuItem* menu_item;
	Gtk::Button* button;

	builder->get_widget("menu_Quit", menu_item);
	menu_item->signal_activate().connect(
		sigc::mem_fun(main_window, &Gtk::Window::close));

	builder->get_widget("menu_NewProject", menu_item);
	menu_item->signal_activate().connect(
		sigc::mem_fun(this, &UI::ShowNewProjectWindow));

	builder->get_widget("button_CancelCreateProject", button);
	button->signal_clicked().connect(
		sigc::mem_fun(new_project_window, &Gtk::Window::close));

	builder->get_widget("button_CreateProject", button);
	button->signal_clicked().connect(
		sigc::mem_fun(this, &UI::CheckCreateProject));
}

void UI::ShowNewProjectWindow(){
	if (!new_project_window) return;

	new_project_window->show();
}

void UI::CheckCreateProject(){
	Gtk::Entry* entry;
	Gtk::FileChooserButton* file_chooser;
	string project_path = "";
	string project_name = "";

	builder->get_widget("entry_NewProjectName", entry);
	project_name = entry->get_text();
	builder->get_widget("file_NewProjectPath", file_chooser);
	project_path = file_chooser->get_filename();

	if (project_name.empty() || project_path.empty()) return;

	project_path.append("/");
	cout << "Create new project: " <<
		project_name << " within " << project_path << endl;

	manager->CreateNewProject(project_path, project_name);
	new_project_window->close();
}

// ########## Destructor
UI::~UI(){
	if (create_class_window)	delete create_class_window;
	if (new_project_window)		delete new_project_window;
	if (main_window)			delete main_window;
	if (manager)				delete manager;
}

// ########## Getters
Gtk::Window&	UI::GetMainWindow()	{ return *main_window; }
bool			UI::isLoaded()		{ return loaded; }
