#include <gtkmm.h>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include "project_manager.hpp"

using namespace std;

ProjectManager::ProjectManager(){
	initialized = false;

	project_path	= "";	project_name	= "";
	resource_file	= "";	compiler_flags	= "";
	linker_flags	= "";	libraries		= "";
}

ProjectManager::~ProjectManager(){}


// ########## ProjectManager functions

void ProjectManager::CreateNewProject(string path, string name){
	project_path = path;
	project_name = name;
	initialized = true;

	GenerateMakefile();
	make_CreateDirStructure();

	SaveResourceToFile(project_path + "sources/",
		"main.cpp", "/template/main.cpp");
}

void ProjectManager::CreateClass(string name){
	SaveResourceToFile(project_path + "sources/",
		name + ".cpp", "/template/source.cpp");

	ReplaceStringInFile("sources/" + name + ".cpp", "__CLASS_NAME__", name);

	SaveResourceToFile(project_path + "headers/",
		name + ".hpp", "/template/header.hpp");

	ReplaceStringInFile("headers/" + name + ".hpp", "__CLASS_NAME__", name);
}


// ########## Helper functions

string ProjectManager::CreateCommand(string cmd){
	if (!initialized) return "";

	return string("cd " + project_path + " && " + cmd);
}

void ProjectManager::ReplaceStringInFile(string file, string match, string replace){
	string cmd("sed -i -- 's/" + match + "/" + replace + "/g' " + file);

	system(CreateCommand(cmd).c_str());
}


// ########## Resource manipulators

string ProjectManager::GetResourceData(string res){
	auto file		= Gio::File::create_for_uri("resource://" + res);
	auto file_info	= file->query_info();
	int file_size	= file_info->get_size();
	char buffer[file_size];

	cout << "Resource \"" << res << "\" size: " << file_size << " bytes" << endl;

	auto stream = file->read();
	stream->read(buffer, file_size);

	buffer[file_size] = '\0';
	return string(buffer);
}

bool ProjectManager::SaveStringToFile(string path, string name, string data){
	ofstream file(path + name, ofstream::trunc);

	if (!file.is_open()){
		cerr << "Failed to truncate file \"" << path + name << "\"" << endl;
		return false;
	}

	file << data;
	file.close();

	return true;
}

bool ProjectManager::SaveResourceToFile(string path, string file, string res){
	string data = GetResourceData(res);
	return SaveStringToFile(path, file, data);
}


// ########## Make commands

void ProjectManager::Make(){
	if (!initialized) return;

	system(CreateCommand("make").c_str());
}

void ProjectManager::Clean(){
	if (!initialized) return;

	system(CreateCommand("make clean").c_str());
}

void ProjectManager::Rebuild(){
	if (!initialized) return;

	Clean();
	Make();
}

void ProjectManager::make_CreateDirStructure(){
	if (!initialized) return;

	system(CreateCommand("make create_dir_structure").c_str());
}


// ########## Makefile functions

void ProjectManager::GenerateMakefile(){
	SaveResourceToFile(project_path, "makefile", "/template/makefile");
	BumpMakefile();
}

void ProjectManager::BumpMakefile(){
	if (!initialized) return;

	ReplaceStringInFile("makefile", "__PROJECT_NAME__", project_name);
	ReplaceStringInFile("makefile", "__RESOURCE_FILE__", resource_file);
	ReplaceStringInFile("makefile", "__COMPILER_FLAGS__", compiler_flags);
	ReplaceStringInFile("makefile", "__LINKER_FLAGS__", linker_flags);
	ReplaceStringInFile("makefile", "__LIBRARIES__", libraries);
}

void ProjectManager::BumpProjectName(){
	if (!initialized) return;

	ReplaceStringInFile("makefile", "__PROJECT_NAME__", project_name);
}

void ProjectManager::BumpResourceFile(){
	if (!initialized) return;

	ReplaceStringInFile("makefile", "__RESOURCE_FILE__", resource_file);
}

void ProjectManager::BumpCompilerFlags(){
	if (!initialized) return;

	ReplaceStringInFile("makefile", "__COMPILER_FLAGS__", compiler_flags);
}

void ProjectManager::BumpLinkerFlags(){
	if (!initialized) return;

	ReplaceStringInFile("makefile", "__LINKER_FLAGS__", linker_flags);
}

void ProjectManager::BumpLibraries(){
	if (!initialized) return;

	ReplaceStringInFile("makefile", "__LIBRARIES__", libraries);
}
