##################################################
#
#	Makefile for a Project Manager
#		by Khin Baptista
#
##################################################

CC		= g++
LINKER	= g++
RCC		= glib-compile-resources

ProjectName		= ProjectManager
ResourceFile	= res.gresource.xml

##################################################

CFLAGS	= -std=c++11 -Wall
LDFLAGS	=

LIBS	= gtkmm-3.0

##################################################

CFLAGS	+= `pkg-config $(LIBS) --cflags`
LDFLAGS	+= `pkg-config $(LIBS) --libs`

##################################################

HeaderPath		= headers
SourcePath		= sources
ObjectPath		= objects
ResourcePath	= resources
ResourceTarget	= $(ResourcePath)/resource.cpp
ResourceObject	= $(ObjectPath)/resource.o

# Collect source and header files
CPP	= $(wildcard $(SourcePath)/*.cpp)
HPP = $(wildcard $(HeaderPath)/*.hpp)

# Each .cpp file produces a .o counterpart
OBJ	= $(patsubst $(SourcePath)/%.cpp, $(ObjectPath)/%.o, $(CPP))

##################################################

.PHONY: all create_dir_structure remove force_remove clean clean_res help

all: $(ProjectName)

resource: $(ResourceObject)

# Compile project
$(ProjectName): $(OBJ) $(ResourceObject)
	$(CC) -o $@ $^ $(LDFLAGS)

# Compile objects
$(ObjectPath)/%.o: $(SourcePath)/%.cpp
	$(CC) -c -o $@ $^ $(CFLAGS) -I$(HeaderPath)

# Compile resource object
$(ResourceObject): $(ResourceTarget)
	$(CC) -c -o $@ $^ $(CFLAGS)

# Parse XML resource into CPP
$(ResourceTarget): $(ResourceFile) $(ResourcePath)/ui.glade
	$(RCC) --target=$@ --sourcedir=$(ResourcePath) --generate-source $(ResourceFile)

##################################################

clean:
	rm -f $(ObjectPath)/*.o $(ResourceTarget) $(ProjectName)

clean_res:
	rm -f $(ResourceTarget)

create_dir_structure:
	mkdir $(HeaderPath) $(SourcePath) $(ObjectPath) $(ResourcePath)

remove:
	rmdir $(HeaderPath) $(SourcePath) $(ObjectPath) $(ResourcePath)

force_remove:
	rm -r $(HeaderPath) $(SourcePath) $(ObjectPath) $(ResourcePath)

##################################################
