#pragma once

#include <string>
#include <vector>
#include <unordered_map>

using namespace std;

class ProjectManager{
	private:
		bool initialized;
		string project_path;
		string project_name;
		string resource_file;
		string compiler_flags;
		string linker_flags;
		string libraries;

		//unordered_map<string, vector<string>> resources;

		string GetResourceData(string resource_name);
		bool SaveStringToFile(string path, string name, string content);
		bool SaveResourceToFile(string path, string name, string resource);

		string CreateCommand(string command);

		/**
			Replace string 'match' with string 'replace' in file.
			File path name is relative to project path.
		*/
		void ReplaceStringInFile(string file, string match, string replace);

		// Bump stuff to existing makefile
		void BumpProjectName();
		void BumpResourceFile();
		void BumpCompilerFlags();
		void BumpLinkerFlags();
		void BumpLibraries();

		void make_CreateDirStructure();

	public:
		ProjectManager();
		virtual ~ProjectManager();

		void CreateNewProject(string path, string name);
		//void OpenProject(string path);

		void CreateClass(string name);
		//void AddResourceList(string prefix);
		//void AddResource(string name, string prefix = "");

		void GenerateMakefile();
		void BumpMakefile();

		void Make();
		void Clean();
		void Rebuild();
};
