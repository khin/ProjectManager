#pragma once

#include <gtkmm.h>
#include <string>
#include "project_manager.hpp"

class UI{
	private:
		Glib::RefPtr<Gtk::Builder> builder;

		Gtk::Window* main_window;
		Gtk::Window* new_project_window;
		Gtk::Window* create_class_window;

		ProjectManager* manager;

		bool loaded;

		void ConnectSignals();

		void ShowNewProjectWindow();

		void CheckCreateProject();

	public:
		UI(std::string resource);
		virtual ~UI();

		Gtk::Window& GetMainWindow();
		bool isLoaded();
};
